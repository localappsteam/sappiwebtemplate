﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Template.Home" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="SiteContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlGrid" runat="server">
                <fieldset style="width: 100%">
                    <legend>
                        <h1>Home</h1>
                    </legend>
                    <asp:Panel ID="PanelContainer" runat="server">

                        <table style="width: 100%; border: none">
                            <tr>
                                <%--<td align="center" style="border: none">--%>
                                <td>
                                    <asp:TextBox ID="txtCompSearch" runat="server"/>
                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtCompSearch"
                                        MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1" CompletionInterval="1000" ServiceMethod="GetCompanies">
                                    </cc1:AutoCompleteExtender>
                                    <asp:Button ID="btnSearchComp" runat="server" Text="Search Company" OnClick="btnSearchComp_Click"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="border: none">
                                    <asp:GridView ID="GridView1" runat="server" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" Width="100%" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound" AllowPaging="True" PageSize="20" OnPageIndexChanging="GridView1_PageIndexChanging">
                                        <AlternatingRowStyle BackColor="#E1E1E1" />
                                        <Columns>
                                            <asp:ButtonField Text="Edit" ButtonType="Button" CommandName="Load">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:ButtonField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </fieldset>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
