﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
//using DAL;
using System.Globalization;
using System.Data.SqlClient;
using System.Configuration;

namespace Template
{
    public partial class Home : System.Web.UI.Page
    {

        //Database_Functions db_functions = new Database_Functions();
        string currentUser = HttpContext.Current.User.Identity.Name.ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //int userId = db_functions.getUserUsingLogon(currentUser);
                // Show the DataTable values in the GridView
                try
                {
                    //GridView1.DataSource = db_functions.getVisitsForGrid(db_functions.getSurnameName(currentUser), db_functions.getRole(userId), null);
                    //GridView1.DataBind();
                }
                catch (Exception ex)
                { }
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Load")
            {
                int rowIndex = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = GridView1.Rows[rowIndex];
                string visitID = row.Cells[1].Text;
                Response.Redirect("AddEdit.aspx?id=" + visitID);
                //string strUrl = "AddEdit.aspx?id=" + visitID;
                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strUrl + "','_blank')", true);
            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[8].Visible = false;
                //e.Row.Cells[6].Text = DateTime.Parse(e.Row.Cells[3].Text).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                //e.Row.Cells[1].Visible = false;
            }

        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //int userId = db_functions.getUserUsingLogon(currentUser);
            //GridView1.PageIndex = e.NewPageIndex;
            //GridView1.DataSource = db_functions.getVisitsForGrid(db_functions.getSurnameName(currentUser), db_functions.getRole(userId), txtCompSearch.Text);
            //GridView1.DataBind();
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetCompanies(string prefixText)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dbconnection"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand("SELECT Name FROM Companies WHERE Name like @Name+'%' ORDER BY Name", con);
            cmd.Parameters.AddWithValue("@Name", prefixText);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            List<string> CountryNames = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                CountryNames.Add(dt.Rows[i][0].ToString());
            }
            con.Close();
            return CountryNames;
        }

        protected void btnSearchComp_Click(object sender, EventArgs e)
        {
            //int userId = db_functions.getUserUsingLogon(currentUser);
            //GridView1.DataSource = db_functions.getVisitsForGrid(db_functions.getSurnameName(currentUser), db_functions.getRole(userId), txtCompSearch.Text);
            //GridView1.DataBind();
        }
    }
}
